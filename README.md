# 202002_first project1


# Kaggler競賽 - Predict Future Sales

Kaggle競賽案例學習 - [Feature engineering, xgboost](https://www.kaggle.com/dlarionov/feature-engineering-xgboost#Part-1,-perfect-features)

Part 1, perfect features¶

`設定顯示最大列數與行數`
```
import numpy as np
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 100)
```
`資料清理套件`
```
from itertools import product
from sklearn.preprocessing import LabelEncoder
```

`繪製圖表套件`
```
import seaborn as sns
import matplotlib.pyplot as plt
%matplotlib inline
```
`模型套件`
```
from xgboost import XGBRegressor
from xgboost import plot_importance
```
`系統套件`
```
import time
import sys
import gc
import pickle
sys.version_info
```

# 模型欄位重要性視覺化
```
def plot_features(booster, figsize):    
    fig, ax = plt.subplots(1,1,figsize=figsize)
    return plot_importance(booster=booster, ax=ax)
```

# 讀取各資料集
```
items = pd.read_csv('../input/items.csv')
shops = pd.read_csv('../input/shops.csv')
cats = pd.read_csv('../input/item_categories.csv')
train = pd.read_csv('../input/sales_train.csv')
# set index to ID to avoid droping it later
test  = pd.read_csv('../input/test.csv').set_index('ID')
```

# Outliers
- [x] 確認item_cnt_day、item_price的box圖，確認資料分佈以及是否有outlier值
```
plt.figure(figsize=(10,4))
plt.xlim(-100, 3000)
sns.boxplot(x=train.item_cnt_day)

plt.figure(figsize=(10,4))
plt.xlim(train.item_price.min(), train.item_price.max()*1.1)
sns.boxplot(x=train.item_price)
```
# 資料前處理

```
# 移除價格及銷售量極端值
  # 價格>100000
  # 銷售量>1000

train = train[train.item_price<100000]
train = train[train.item_cnt_day<1001]

# 價格<0的品項用中位數取代
median = train[(train.shop_id==32)&(train.item_id==2973)&(train.date_block_num==4)&(train.item_price>0)].item_price.median()
train.loc[train.item_price<0, 'item_price'] = median

# 合併重複的商店名稱
train.loc[train.shop_id == 0, 'shop_id'] = 57
test.loc[test.shop_id == 0, 'shop_id'] = 57

train.loc[train.shop_id == 1, 'shop_id'] = 58
test.loc[test.shop_id == 1, 'shop_id'] = 58

train.loc[train.shop_id == 10, 'shop_id'] = 11
test.loc[test.shop_id == 10, 'shop_id'] = 11
```

# 商店/商品品項處理
```
# shop_name 以城市名稱開頭
# 商品類型包含大類及子類

# 把 shop_name 用空白分割取第一個字作為城市
# 把城市名稱作 label_encode

shops.loc[shops.shop_name == 'Сергиев Посад ТЦ "7Я"', 'shop_name'] = 'СергиевПосад ТЦ "7Я"'
shops['city'] = shops['shop_name'].str.split(' ').map(lambda x: x[0])
shops.loc[shops.city == '!Якутск', 'city'] = 'Якутск'
shops['city_code'] = LabelEncoder().fit_transform(shops['city'])
shops = shops[[

# 把 item_category_name 用-分割取第一個字作為類別 第二個字作為子類別
# 類別/子類別都作 label_encode

cats['split'] = cats['item_category_name'].str.split('-')
cats['type'] = cats['split'].map(lambda x: x[0].strip())
cats['type_code'] = LabelEncoder().fit_transform(cats['type'])

# if subtype is nan then type
cats['subtype'] = cats['split'].map(lambda x: x[1].strip() if len(x) > 1 else x[0].strip())
cats['subtype_code'] = LabelEncoder().fit_transform(cats['subtype'])
cats = cats[['item_category_id','type_code', 'subtype_code']]

items.drop(['item_name'], axis=1, inplace=True)
```

# Monthly sales
```
# 檢視test set跟training set不同的item_id
len(list(set(test.item_id) - set(test.item_id).intersection(set(train.item_id)))), len(list(set(test.item_id))), len(test)

# 列出每個月不同商店/銷售商品
ts = time.time()
matrix = []
cols = ['date_block_num','shop_id','item_id']
for i in range(34):
    sales = train[train.date_block_num==i]
    matrix.append(np.array(list(product([i], sales.shop_id.unique(), sales.item_id.unique())), dtype='int16'))

# 把 array 垂直堆疊轉成 dataframe
matrix = pd.DataFrame(np.vstack(matrix), columns=cols)
matrix['date_block_num'] = matrix['date_block_num'].astype(np.int8)
matrix['shop_id'] = matrix['shop_id'].astype(np.int8)
matrix['item_id'] = matrix['item_id'].astype(np.int16)
matrix.sort_values(cols,inplace=True)
time.time() - ts

# 計算每月營收
train['revenue'] = train['item_price'] *  train['item_cnt_day']


# 計算每商店每商品每月銷售量
# 產出月/店/商品/銷售量資料集matrix

ts = time.time()
group = train.groupby(['date_block_num','shop_id','item_id']).agg({'item_cnt_day': ['sum']})
group.columns = ['item_cnt_month']
group.reset_index(inplace=True)

matrix = pd.merge(matrix, group, on=cols, how='left')
matrix['item_cnt_month'] = (matrix['item_cnt_month']
                                .fillna(0)
                                .clip(0,20) # NB clip target here
                                .astype(np.float16))
time.time() - ts
```
